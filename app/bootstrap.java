import play.jobs.Job;
import play.jobs.OnApplicationStart;
import play.test.Fixtures;

import java.util.List;

import play.*;
import play.jobs.*;
import play.test.*;
 
import models.*;


@OnApplicationStart
public class bootstrap extends Job 
{ 
  public void doJob(){
    if (User.count() == 0) {
      Fixtures.loadModels("data.yml");
    }
  }
}





/*@OnApplicationStart
public class bootstrap extends Job {
	public void doJob() {
		Fixtures.deleteDatabase();
		Fixtures.loadModels("data.yml"); // to preload data
	}
}*/