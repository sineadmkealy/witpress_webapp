package models;

import java.util.Date;
import javax.persistence.Entity;

import javax.persistence.ManyToOne;

import play.db.jpa.Model;

/**
 * Contains the business logic and data fields required by the comment model
 * 
 * @author Sinead
 *
 */
@Entity // indicates information to be stored to a database
public class Comment extends Model {
	public String content;
	public Date dateComment;

/*	@ManyToOne
	public User from; // one user can enter many comments into a post...???
	// NOT RELEVANT: instead the [many comments to one post[ mapping contained in post class
*/
	/**
	 * The content of a comment
	 * 
	 * @param content
	 *            the text contained within the comment
	 */
	public Comment(String content) {
		// this.from = from;
		this.content = content;
	}

}
