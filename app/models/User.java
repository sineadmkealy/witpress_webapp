package models;

import javax.persistence.Entity;
import play.db.jpa.Model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;

@Entity // this User class mapped to a table in the database
public class User extends Model {
	public String firstName;
	public String lastName;
	public String email;
	public String password;

	/*
	 * User can enter many separate texts into blog
	 * 
	 * @OneToMany(mappedBy = "from", cascade = CascadeType.ALL) public
	 * List<Post> posts; = new ArrayList<Post>(); // to allow for dynamic posts
	 */
	// One User can enter many separate texts (posts) into blog
	@OneToMany(cascade = CascadeType.ALL)
	public List<Post> posts;

	/*
	 * // User can enter many separate texts into blog
	 * 
	 * @OneToMany(mappedBy = "post" , cascade = CascadeType.ALL)
	 * List<Comment>comments = new ArrayList<Comment>(); // to allow for dynamic
	 * comments
	 */
	/*
	 * // For preloaded data...????: // User can enter many separate texts into
	 * blog
	 * 
	 * @OneToMany(mappedBy = "post", cascade = CascadeType.ALL)
	 * List<Comment>comments; // to load preloaded comments????
	 */

	/**
	 * Establishes the business logic and data required by the User model
	 * 
	 * @param firstName
	 *            the User first name
	 * @param lastName
	 *            the User last name
	 * @param email
	 *            the user's email
	 * @param password
	 *            the user's login password
	 */
	public User(String firstName, String lastName, String email, String password) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;

	}

	/**
	 * Method to identify the User by reference to their email address
	 * 
	 * @param email
	 *            the email address actual parameter passed in by the user to
	 *            search by
	 * @return returns the user email in the first instance
	 */
	public static User findByEmail(String email) {
		return find("email", email).first();
	}

	/**
	 * Method to check that the actual parameter email address submitted matches
	 * that of the User email passed in at signup
	 * 
	 * @param password
	 * @return returns true if match exits or false if no match
	 */
	public boolean checkPassword(String password) {
		return this.password.equals(password);
	}

	/** ALTERANTIVE CHECK BY USER EMAIL ONLY......???? */
	/**
	 * For simplicity, we could check if 2 User objects equal, ie the same user,
	 * if their emails match.
	 * 
	 * @param Object
	 *            (a User object) to be compared to this object
	 * @return True if the email of this object matches the argument's email.
	 */
	/*
	 * public boolean equals (Object o) { return
	 * email.equalsIgnoreCase(((User)o).email); }
	 */

	/**
	 * Adds a post object to User
	 * 
	 * @param post
	 *            Any text the Blog owner may enter into the blog, ie. to add a
	 *            Post object.
	 */
	public void addPost(Post post) {
		posts.add(post);

	}

/*	//IN SOLUTION..????
	  public String toString()
	  {
	    return firstName;
	  }
	}*/
}
