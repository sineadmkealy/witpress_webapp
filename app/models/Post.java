package models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
//import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import play.db.jpa.Model;

import java.util.Date;
import java.util.List;

@Entity // indicates information to be stored to a database
public class Post extends Model {
	public String title;
	public String content; // @Lob for large bodies of text
	public Date datePost;

/*	@ManyToOne
	public User from; // one user can enter many posts into a blog...???
*/
	// many comments can be attached by authorised users to each post object
	// ie.  there will be many Comment objects for each Post object.
	@OneToMany(cascade = CascadeType.ALL)
	public List<Comment> comments;

	/**
	 * Any text the owner may enter in the (post)blog
	 * 
	 * @param from
	 * @param title
	 * @param content
	 */
	public Post(String title, String content) {
		this.title = title;
		this.content = content;
		datePost = new Date();
	}

	/**
	 * Adds comment object to a post
	 * 
	 * @param comment
	 *            Any text that an authorised user may attach to a post
	 */
	public void addComment(Comment comment) {
		comments.add(comment);
	}

}
