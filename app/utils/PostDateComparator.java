package utils;

import java.util.Comparator;

import models.Post;

public class PostDateComparator implements Comparator<Post> {
	/**
	 * compare the time-date attributes of each message use the Date compareTo
	 * method use appropriate attribute of Message b as the parameter
	 */

	@Override
	public int compare(Post a, Post b) {
		return a.datePost.compareTo(b.datePost);

	}
}
