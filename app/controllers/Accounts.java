package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

/**
 * An Accounts class to inherit data and behaviour from the Controller class. An
 * extension of the Play framework, the controller contains methods/actions that
 * process requests and send results to the client (the web page). The
 * controller is mapped to views subfolders, which contains a template (eg.
 * index template - index.html) corresponding to each method in the controller.
 * 
 * @author Sinead
 * @version 2.0 June 2016
 * 
 */
public class Accounts extends Controller {

	public static void index() {
		render();
	}

	/**
	 * Renders the Accounts signup view
	 */
	public static void signup() {
		render();
	}

	/**
	 * Registers the signed-up user and saves to database
	 * 
	 * @param user
	 *            Simulates the logged-in user
	 */
	public static void register(User user) {
		user.save();
		Logger.info(user.firstName + " " + user.lastName + " registered");
		signin();
	}

	/**
	 * Creates the user signin for rendering in the Accounts login view
	 */
	public static void signin() {
		// Logger.info(user.firstName + " " + user.lastName + " signed in");
		render();
	}

	/**
	 * Creates the user signout functionality by diverting the User to the
	 * signin page
	 */
	public static void signout() {
		session.clear();
		Logger.info("User signed out");
		signup();
	}

	/**
	 * Authenticate the user by checking their email and passsword previously
	 * registered as User. Assigns at UserId for the User for the current
	 * session.
	 */
	public static void authenticate(String email, String password) {
		Logger.info("Attempting to authenticate with " + email + " , password " + password);
		User user = User.findByEmail(email);
		if ((user != null) && (user.checkPassword(password) == true)) {
			Logger.info("Succesful User authentification");
			session.put("logged_in_userid", user.id);
			// diverts to Blog page if successfully logged-in
			Blog.index();
			// PublicBlogDirectory.index(); // Alt.: divert to Blog Directory if
			// successfully logged-in..?
		} else {
			Logger.info("Authentification failed");
			signin();
		}
	}

	/**
	 * Affirms the current user by searching the logged-in UserId of the User
	 * for the current session
	 * 
	 * @return the currently logged-in User
	 **/
	public static User getLoggedInUser() {
		User user = null;
		if (session.contains("logged_in_userid")) {
			String userId = session.get("logged_in_userid");
			user = User.findById(Long.parseLong(userId));
		} else {
			index(); // => log in
		}
		return user;
	}
}
