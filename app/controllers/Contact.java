package controllers;
import play.mvc.*;

/**
 *  An Accounts class to inherit data and behaviour from the Controller class.
 *  This contains methods/actions that process requests and send results to the 
 *  client - Contact web page.
 * @author Sinead
 *
 */
public class Contact extends Controller {

	/**
	 * Method to render the Contact html page.
	 */
	public static void index() {
		render();
	}
}
