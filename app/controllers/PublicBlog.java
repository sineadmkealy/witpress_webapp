package controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

import models.Comment;

import models.Post;
import models.User;
import play.Logger;
import play.mvc.Controller;

/**
 * The PublicBlog controller renders a specific blog. The blog's user is determined 
 * by the incoming user id received from the view.
 * 
 * @author Sinead
 *
 */
public class PublicBlog  extends Controller {
	
	/**
	 * Renders the Public Blog nothwithstanding whether the user a logged-in user
	 * @param id The User id
	 */
	 public static void show(Long id) {     
	    Logger.info("Request to show blogs for user id: " + id);
	    User user = User.findById(id);
	    List<Post> reversePosts  = new ArrayList<Post> (user.posts);
	    Collections.reverse(reversePosts);
	    
/*		List<Post> posts = new ArrayList<Post>(user.posts);
		// Returns a comparator that imposes the reverse of the natural ordering (date) on a 
		//collection of objects that implement the Comparable interface.
		Collections.reverseOrder();
		Logger.info("Sorting posts by date, in reverse order: most recent first");
		render(user, posts);*/

	    User loggedInUser = null;
	    if (session.contains("logged_in_userid")) {
	      String userId = session.get("logged_in_userid");
	      loggedInUser = User.findById(Long.parseLong(userId));
	    }
	    render(user, loggedInUser, reversePosts);
	    //render(user, loggedInUser, posts);
	  } 

	 /**
	  * 
	  * Renders a User comment
	  * @param postid The post id
	  * @param userID The user id
	  * @param loggedInUserID If user a logged-in user,their id
	  * @param content The content of the comment 
	  */	 
	    public static void newComment(Long postid, Long userID, Long loggedInUserID, String content)
	    {    
	      Logger.info("Post ID = " + postid);
	      Post post = Post.findById(postid);
	      Comment comment = new Comment(content);

	      post.addComment(comment);
	      post.save();
	      show(userID);
	    } 
	
}