package controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

import models.Comment;
import models.Post;
import models.User;
import play.Logger;
import play.mvc.Controller;

/**
 * The PublicBlogDirectory lists all users who have created at least one blog post.
 * @author Sinead
 *
 */
public class PublicBlogDirectory  extends Controller {
/**
 * Renders the PublicBlogDirectory view
 */
  public static void index() {
    ArrayList<User> blogAuthors = new ArrayList<User>();

    List<User> users = User.findAll();
    for (User user : users) {
      if (user.posts.size() > 0) {
    	  blogAuthors.add(user);
      }
    }
    render(blogAuthors);
  }
}