package controllers;

import java.util.List;
import models.Post;
import models.User;
import play.Logger;
import play.mvc.Controller;
import utils.PostDateComparator;

import java.util.Collections;
import java.util.ArrayList;

/**
 * The Blog class to inherit data and behaviour from the Controller class. This
 * displays the list of posts and allows for the creation and deletion of a post.
 * 
 * @author Sinead
 *
 */
public class Blog extends Controller {

	/**
	 * Renders the Blog view
	 */
	public static void index() {
		// no need for if else statement whether user logged in as user must be
		// logged in to access Blog.
		User user = Accounts.getLoggedInUser();
		//Logger.info("Logged-in user is: " + user.firstName + " " + user.lastName);

		
		//SOLUTION
		// to sort the posts in reverse order, most recent first
		// creates an array list of the selected user's post objects to generate
		// a reverse order list
	    List<Post> reversePosts  = new ArrayList<Post> (user.posts);
		Collections.reverse(reversePosts); 
		// renders this list for the user object, into the view
		render(user, reversePosts); 
	}
		
/*		List<Post> posts = new ArrayList<Post>(user.posts);
		// Returns a comparator that imposes the reverse of the natural ordering (date) on a 
		//collection of objects that implement the Comparable interface.
		Collections.reverseOrder();
		Logger.info("Sorting posts by date, in reverse order: most recent first");
		render(user, posts);*/
		
	
 /**
  * Creates a new post in a blog
  * @param title
  * @param content
  */
	public static void newPost(String title, String content) {
		User user = Accounts.getLoggedInUser();//to ascertain the current user
		
		Post post = new Post(title, content);
				user.addPost(post); // refers to addPost method in User model
		user.save();
		Logger.info("User: " + user + "Post title: " + title + " ,content: " + content + " added");
		index(); //refreshes the page
	}
 /**
  * Deletes a post in a Blog
  * @param postid
  */
	public static void deletePost(Long postid){
		User user = Accounts.getLoggedInUser(); //to ascertain the current user
		
		Post post = Post.findById(postid); // to find the post
		user.posts.remove(post); // remove post from the User's collection of posts (ArrayList)
		
		user.save(); // refers to the database: saves user updated status, with post removed
		post.delete(); // delete post from the database
		index(); //refreshes the page
}
	
}
