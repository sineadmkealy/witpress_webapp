package controllers;

import java.util.List;

import models.Comment;
import models.Post;
import models.User;
import play.Logger;
import play.mvc.Controller;
import java.util.Collections;
import java.util.ArrayList;

/**
 * This class manages comments associated with a specified post. It allows for
 * the creation and deletion of new comments associated with the relevant post
 * 
 * @author Sinead
 *
 */
public class BlogPost extends Controller {
/**
 * To unveil a separate page showing comments associated with selected posts
 * @param postid
 */
	public static void show(Long postid){
		Post post = Post.findById(postid);
		Logger.info("Displaying Post Id: " + postid);
		render(post);
	}
	
	public static void newComment(Long postid, String content){
		Post post = Post.findById(postid);
		Logger.info("Displaying Post Id: " + postid);
		
		Comment comment = new Comment(content);
		// refers to addComment method in Post model:
		post.addComment(comment); // refers to the database
		post.save(); // refers to the database
		show(postid);
		Logger.info("Adding new comment: " + content);
	}
	
	public static void deleteComment(Long postid, Long commentid){
	Logger.info("Post: " + postid + "comment: " + commentid + "to be deleted");
    Comment comment = Comment.findById(commentid);
    Post post = Post.findById(postid);
	
    post.comments.remove(comment);
    post.save();
	// display the updated comment list where comment deleted:
    show(postid);
	}
}
